import { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import SendIcon from '@mui/icons-material/Send';
import CloseIcon from '@mui/icons-material/Close';
import TextField from "@mui/material/TextField";
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import axiosInstance from "../../service/axios";
import { useForm } from "react-hook-form";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

export default function EditComponentBook(props) {
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const {
        register,
        // eslint-disable-next-line no-unused-vars
        handleSubmit,
        // eslint-disable-next-line no-unused-vars
        formState: { errors, isSubmitting },
    } = useForm();
    const onEditSubmit = async (values) => {
        setLoading(true);
        try {
            await axiosInstance.put(`/students/${props.id}/`, values);
            setLoading(false);
            handleClose();
            props.fetchBooks();
        } catch (error) {
            alert(error);
            setLoading(false)
        }
    };

    return (
        <>
            <IconButton aria-label="delete" size="small" onClick={handleClickOpen} color="secondary">
                <EditIcon fontSize="small" />
            </IconButton>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Update student
                </BootstrapDialogTitle>
                <form onSubmit={handleSubmit(onEditSubmit)}>
                    <DialogContent dividers>
                        <Container>
                            <Box
                                sx={{
                                    width: 500,
                                    height: 250,
                                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                                }}
                            >
                                <TextField
                                    type="text"
                                    label="first name"
                                    variant="outlined"
                                    id="FirstName"
                                    name="FirstName"
                                    fullWidth
                                    {...register("FirstName", {
                                        required: " Firstname required",
                                        minLength: {
                                            value: 2,
                                            message: "titre au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 20,
                                            message: "Titre au maximum 20 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.FirstName)}
                                    helperText={errors.FirstName?.message}
                                />
                                <TextField
                                    type="text"
                                    label="last name"
                                    variant="outlined"
                                    id="LastName"
                                    name="LastName"
                                    fullWidth
                                    {...register("LastName", {
                                        required: "last name obligatoire",
                                        minLength: {
                                            value: 2,
                                            message: "auteur au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 20,
                                            message: "auteur au maximum 20 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.LastName)}
                                    helperText={errors.LastName?.message}
                                />
                                <TextField
                                    type="text"
                                    label="registration"
                                    variant="outlined"
                                    id="RegistrationNo"
                                    name="RegistrationNo"
                                    fullWidth
                                    {...register("RegistrationNo", {
                                        required: "required",
                                        minLength: {
                                            value: 2,
                                            message: "auteur au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 20,
                                            message: "auteur au maximum 20 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.RegistrationNo)}
                                    helperText={errors.RegistrationNo?.message}
                                />

                                <TextField
                                    type="text"
                                    label="email"
                                    variant="outlined"
                                    id="Email"
                                    name="Email"
                                    fullWidth
                                    {...register("Email", {
                                        required: "required",
                                        minLength: {
                                            value: 2,
                                            message: "auteur au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 20,
                                            message: "auteur au maximum 20 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.Email)}
                                    helperText={errors.Email?.message}
                                />

                                <TextField
                                    type="text"
                                    label="course"
                                    variant="outlined"
                                    id="Course"
                                    name="Course"
                                    fullWidth
                                    {...register("Course", {
                                        required: "required",
                                        minLength: {
                                            value: 2,
                                            message: "auteur au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 20,
                                            message: "auteur au maximum 20 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.Course)}
                                    helperText={errors.Course?.message}
                                />
                            </Box>
                        </Container>
                    </DialogContent>
                    <DialogActions>
                        {loading ? (
                            <Button>
                                Loading...
                            </Button>
                        ) : (<Button variant='outlined' endIcon={<SendIcon />} type="submit">
                            Update
                        </Button>)}
                    </DialogActions>
                </form>
            </BootstrapDialog>
        </>
    );
}