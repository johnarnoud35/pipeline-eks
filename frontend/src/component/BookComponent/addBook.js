import { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import SendIcon from '@mui/icons-material/Send';
import TextField from "@mui/material/TextField";
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import AddIcon from '@mui/icons-material/Add';
//import LoadingButton from '@mui/lab/LoadingButton';
import SaveIcon from '@mui/icons-material/Save';
import axiosInstance from "../../service/axios";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function AddComponentBook({ fetchBooks }) {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm();

  const onAddSubmit = async (values) => {
    setLoading(true);
    try {
      const response = await axiosInstance.post("/students/", values);
      setLoading(false);
      handleClose();
      Swal.fire("Succès!", "Ajout avec succès!", "success");
      fetchBooks(); // ajoute le nouveau livre au début de la liste existante
    } catch (error) {
      console.error("Erreur lors de l'ajout du livre :", error);
      setLoading(false);
    }
  };
  
  



  return (
    <div>
      <Button variant="contained"  endIcon={<AddIcon />} onClick={handleClickOpen}>
        demostration
      </Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add Student
        </BootstrapDialogTitle>
        <form onSubmit={handleSubmit(onAddSubmit)}>
          <DialogContent dividers>
            <Container>
              <Box
                sx={{
                  width: 500,
                  height: 250,
                  '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
              >
                <TextField
                  type="text"
                  label="first name"
                  variant="outlined"
                  id="FirstName"
                  name="FirstName"
                  fullWidth
                  {...register("FirstName", {
                    required: " Firstname required",
                    minLength: {
                      value: 2,
                      message: "titre au moin 2 caractères",
                    },
                    maxLength: {
                      value: 20,
                      message: "Titre au maximum 20 caractères",
                    },
                  })}
                  error={Boolean(errors.FirstName)}
                  helperText={errors.FirstName?.message}
                />
                <TextField
                  type="text"
                  label="last name"
                  variant="outlined"
                  id="LastName"
                  name="LastName"
                  fullWidth
                  {...register("LastName", {
                    required: "last name obligatoire",
                    minLength: {
                      value: 2,
                      message: "auteur au moin 2 caractères",
                    },
                    maxLength: {
                      value: 20,
                      message: "auteur au maximum 20 caractères",
                    },
                  })}
                  error={Boolean(errors.LastName)}
                  helperText={errors.LastName?.message}
                />
                <TextField
                  type="text"
                  label="registration"
                  variant="outlined"
                  id="RegistrationNo"
                  name="RegistrationNo"
                  fullWidth
                  {...register("RegistrationNo", {
                    required: "required",
                    minLength: {
                      value: 2,
                      message: "auteur au moin 2 caractères",
                    },
                    maxLength: {
                      value: 20,
                      message: "auteur au maximum 20 caractères",
                    },
                  })}
                  error={Boolean(errors.RegistrationNo)}
                  helperText={errors.RegistrationNo?.message}
                />

                <TextField
                  type="text"
                  label="email"
                  variant="outlined"
                  id="Email"
                  name="Email"
                  fullWidth
                  {...register("Email", {
                    required: "required",
                    minLength: {
                      value: 2,
                      message: "auteur au moin 2 caractères",
                    },
                    maxLength: {
                      value: 20,
                      message: "auteur au maximum 20 caractères",
                    },
                  })}
                  error={Boolean(errors.Email)}
                  helperText={errors.Email?.message}
                />

                <TextField
                  type="text"
                  label="course"
                  variant="outlined"
                  id="Course"
                  name="Course"
                  fullWidth
                  {...register("Course", {
                    required: "required",
                    minLength: {
                      value: 2,
                      message: "auteur au moin 2 caractères",
                    },
                    maxLength: {
                      value: 20,
                      message: "auteur au maximum 20 caractères",
                    },
                  })}
                  error={Boolean(errors.Course)}
                  helperText={errors.Course?.message}
                />
              </Box>
            </Container>
          </DialogContent>
          <DialogActions>
            {loading ? (
              <Button>
                Loading...
              </Button>
            ) : (<Button variant='outlined' endIcon={<SendIcon />} type="submit">
              Save
            </Button>)}

          </DialogActions>
        </form>
      </BootstrapDialog>
    </div>
  );
}