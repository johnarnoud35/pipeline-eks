import React, { useState, useRef, useEffect } from "react";
import axiosInstance from "../../service/axios";
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import CircularProgress from '@mui/material/CircularProgress';
import Swal from "sweetalert2";
import AddComponentBook from "./addBook";
import EditComponentBook from "./editBook";

export const Book = () => {
    const [books, setBooks] = useState([]);
    const [loading, setLoading] = useState(true);
    const [deletingIds, setDeletingIds] = useState([]);
    const isMounted = useRef(false);

    useEffect(() => {
        if (isMounted.current) return;
        fetchBooks();
        isMounted.current = true;
    }, []);



    const fetchBooks = async () => {
        setLoading(true);
        try {
            const res = await axiosInstance.get("/students");
            setBooks(res.data);
        } catch (error) {
            console.error(error);
            Swal.fire("Erreur", error.toString(), "error");
        } finally {
            setLoading(false);
        }
    }

    const deleteTodo = (id) => {
        // Ajoute l'identifiant à la liste des éléments en cours de suppression
        setDeletingIds((prevIds) => [...prevIds, id]);

        axiosInstance
            .delete(`/students/${id}`)
            .then(() => {
                Swal.fire("Supprimé!", "", "success");
                // Retire l'identifiant de la liste des éléments en cours de suppression
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
                // Retirer l'élément supprimé du tableau
                setBooks((prevBooks) => prevBooks.filter((book) => book.studentId !== id));
            })
            .catch((error) => {
                Swal.fire("Erreur", error.toString(), "error");

                // Retire l'identifiant de la liste des éléments en cours de suppression
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
            });
    };

    function Supprimer(id) {
        Swal.fire({
            title: "Etes-vous sûre?",
            text: "Cette action est irreversible!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            cancelButtonText: "Annuler",
            confirmButtonText: "Supprimer",
        }).then((result) => {
            if (result.isConfirmed) {
                deleteTodo(id);
            }
        });
    }

    return (
        <Box component="main" sx={{ p: 3 }}>
            <Toolbar />
            <AddComponentBook fetchBooks={fetchBooks} setBooks={setBooks} />
            <br />
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="center">First Name</TableCell>
                            <TableCell align="center">Last Name</TableCell>
                            <TableCell align="center">Registration N°</TableCell>
                            <TableCell align="center">Email</TableCell>
                            <TableCell align="center">Cours</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <TableRow>
                                <TableCell colSpan={7} align="center">Loading...</TableCell>
                            </TableRow>
                        ) : (books.length === 0 ? (
                            <TableRow>
                                <TableCell colSpan={7} align="center">Aucune donnée pour l'instant.</TableCell>
                            </TableRow>
                        ) : (
                            books.map((book, index) => (
                                <TableRow
                                    key={index}  // Ajoutez cette ligne pour définir une clé unique
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {book.studentId}
                                    </TableCell>
                                    <TableCell align="center">{book.FirstName}</TableCell>
                                    <TableCell align="center">{book.LastName}</TableCell>
                                    <TableCell align="center">{book.RegistrationNo}</TableCell>
                                    <TableCell align="center">{book.Email}</TableCell>
                                    <TableCell align="center">{book.Course}</TableCell>
                                    <TableCell align="center">
                                        <EditComponentBook
                                            id={book.studentId}
                                            firstname={book.FirstName}
                                            lastname={book.LastName}
                                            registrationNo={book.RegistrationNo}
                                            email={book.Email}
                                            course={book.Course}
                                            fetchBooks={fetchBooks}
                                        />
                                        <IconButton
                                            aria-label="delete"
                                            size="small"
                                            color="error"
                                            onClick={() => Supprimer(book.studentId)}
                                        >
                                            {deletingIds.includes(book.studentId) ? (
                                                <CircularProgress color="secondary" size={20} />
                                            ) : (
                                                <DeleteIcon fontSize="small" />
                                            )}
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))
                        )
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
}
