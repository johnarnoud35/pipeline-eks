import { useState, useRef, useEffect } from "react";
import axiosInstance from "../../service/axios";
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Swal from "sweetalert2";

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

export const Note = () => {
    const [books, setBooks] = useState([]);
    const [loading, setLoading] = useState(true);
    const isMounted = useRef(false);

    useEffect(() => {
        if (isMounted.current) return;
        fetchBooks();
        isMounted.current = true
    }, []);

    const fetchBooks = async => {
        setLoading(true);
        axiosInstance.get("/books")
            .then((res) => {
                setBooks(res.data)
                console.log(res.data)
            }).catch((error) => {
                console.error(error)
            }).finally(() => {
                setLoading(false)
            })
    }

    const rows = books;

    const deleteTodo = (id) => {
        axiosInstance
            .delete(`/books/${id}`)
            .then(() => {
                Swal.fire("Supprimé!", "", "success");
                fetchBooks();
            })
            .catch((error) => Swal.fire("Erreur", error.toString(), "error"));
    };

    function Supprimer(id) {
        Swal.fire({
            title: "Etes-vous sûre?",
            text: "Cette action est irreversible!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            cancelButtonText: "Annuler",
            confirmButtonText: "Supprimer",
        }).then((result) => {
            if (result.isConfirmed) {
                deleteTodo(id);
            }
        });
    }

    return (
        <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
        <Typography>
            Lorem Lorem Note
        </Typography>
      </Box>


    );

}