import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Outlet,
} from "react-router-dom";
import {Book} from "./component/BookComponent/book";
import {Note} from "./component/NoteComponent/Note";
import {Etudiant} from "./component/EtudiantComponent/etudiant";
import {Matiere} from "./component/MatiereComponent/Matiere";
import {NavBar} from "./component/NavBarComponent/NavBar";

const Layout = () => {
  return (
    <>
      <NavBar />
      <Outlet />
    </>
  );
};

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Book />,
      },
      {
        path: "/note",
        element: <Note />,
      },
      {
        path: "/matiere",
        element: <Matiere />,
      },
      {
        path: "/etudiant",
        element: <Etudiant />,
      },
    ],
  },
  {
    path: "/register",
    element: <p>register</p>,
  },
  {
    path: "/login",
    element: <p>login</p>,
  },
]);

function App() {
  return (
    <>
      <>
        <RouterProvider router={router} />
      </>
    </>
  );
}

export default App;