import axios from "axios";

const baseURL = "http://ec2-44-214-44-188.compute-1.amazonaws.com:32260";

const axiosInstance = axios.create({
  baseURL,
});

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error)
);

export default axiosInstance;